import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-character',
  templateUrl: './character.component.html',
  styleUrls: ['./character.component.scss']
})
export class CharacterComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }



  character = {
    name:"",
    imgUrl:"",
    atack: 0,
    defense:0
  }

}
