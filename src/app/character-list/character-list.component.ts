import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-character-list',
  templateUrl: './character-list.component.html',
  styleUrls: ['./character-list.component.scss']
})
export class CharacterListComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  @Input() character:Character = {
    id:0,
    name:"",
    description:"",
    avatar:"",
    idPlanet:0

  };
 



}

interface Character{
  id:number,
  name:string,
  description:string,
  avatar:string,
  idPlanet:number
}