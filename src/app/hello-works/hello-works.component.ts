import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hello-works',
  templateUrl: './hello-works.component.html',
  styleUrls: ['./hello-works.component.scss']
})
export class HelloWorksComponent implements OnInit {

  text:string = "Hola, soy un texto"
  edad:number = 18;

  constructor() { }

  ngOnInit(): void {
  }

}
