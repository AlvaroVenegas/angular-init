import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-character-prueba',
  templateUrl: './character-prueba.component.html',
  styleUrls: ['./character-prueba.component.scss']
})
export class CharacterPruebaComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  characters:Character[] = [{
    id:0,
    name:"",
    description:"",
    avatar:"",
    idPlanet:0

  }];



}

interface Character{
  id:number,
  name:string,
  description:string,
  avatar:string,
  idPlanet:number
}
/*
async function getPlanets() {
  const res = await fetch('http://localhost:3000/planets')
  const resData = await res.json()
  printPlanets(resData);
  */