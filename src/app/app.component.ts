import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular-init';

  dataCard1 = {
    imgUrl : "https://upload.wikimedia.org/wikipedia/commons/thumb/3/36/%D0%9F%D0%B8%D1%82%D0%BE%D0%BC%D0%BD%D0%B8%D0%BA_%22%D0%90%D1%81%D1%81%D0%BE%D0%BD_%D0%90%D1%80%D0%B8%22_%D0%A6%D0%B2%D0%B5%D1%80%D0%B3%D1%88%D0%BD%D0%B0%D1%83%D1%86%D0%B5%D1%80%D1%8B_%D0%B1%D0%B5%D0%BB%D1%8B%D0%B5_%D0%B8_%D1%87%D0%B5%D1%80%D0%BD%D1%8B%D0%B5_%D1%81_%D1%81%D0%B5%D1%80%D0%B5%D0%B1%D1%80%D0%BE%D0%BC_12.jpg/1200px-%D0%9F%D0%B8%D1%82%D0%BE%D0%BC%D0%BD%D0%B8%D0%BA_%22%D0%90%D1%81%D1%81%D0%BE%D0%BD_%D0%90%D1%80%D0%B8%22_%D0%A6%D0%B2%D0%B5%D1%80%D0%B3%D1%88%D0%BD%D0%B0%D1%83%D1%86%D0%B5%D1%80%D1%8B_%D0%B1%D0%B5%D0%BB%D1%8B%D0%B5_%D0%B8_%D1%87%D0%B5%D1%80%D0%BD%D1%8B%D0%B5_%D1%81_%D1%81%D0%B5%D1%80%D0%B5%D0%B1%D1%80%D0%BE%D0%BC_12.jpg",
    text : "Perroton",
    textButton : "insertar Perro"
    
  };

  characters: Character[] = [
    
    {
      id: 1,
      name: "Goku",
      description: "Son Goku (孫そん悟ご空くうKan, Son GokūHepJP, Sūn Wùkōng), originalmente llamado Zero en Estados Unidos e Hispanoamérica (doblaje de Harmony Gold) y posteriormente Gokú en este último, es el protagonista principal del manga y anime de Dragon Ball creado por Akira Toriyama. Su nombre real y de nacimiento es Kacarrot (カカロットKan, KakarottoHepJP, Kakarrot en Saiyan.png en alfabeto saiyano) y es uno de los pocos saiyanos que lograron sobrevivir a la destrucción total del Planeta Vegeta del Universo 7. Es el segundo hijo de Bardock y Gine, hermano menor de Raditz, nieto adoptivo de Son Gohan, esposo de Chi-Chi, padre de Son Gohan y Son Goten, a su vez también es el abuelo de Pan y ancestro de Son Goku Jr.",
      avatar: "https://sm.ign.com/t/ign_es/screenshot/default/c66bfac9bc20_emhq.1280.jpg",
      idPlanet: 1
    },
    {
      id: 2,
      name: "Vegeta",
      description: "Vegeta IV (ベジータ四世Kan, Bejīta Yon-sei[27]HepJP), reconocido oficialmente como el Príncipe Vegeta (ベジータ王子Kan, Bejīta-ōjiHepJP) y más conocido simplemente como Vegeta (ベジータKan, BejītaHepJP) o Vegeeta, es el deuteragonista de Dragon Ball Z, Dragon Ball Z Kai, Dragon Ball GT y Dragon Ball Super. Es el hijo mayor del Rey Vegeta III, así como el príncipe más reciente de la familia real saiyana y uno de los pocos supervivientes tras el genocidio saiyano del planeta Vegeta del Universo 7, destruido a manos de Freeza. Es el eterno rival de Son Goku, hermano mayor de Tarble, el esposo de Bulma, padre de Trunks y Bra y ancestro de Vegeta Jr.",
      avatar: "https://sm.ign.com/t/ign_es/screenshot/default/c66bfac9bc20_emhq.1280.jpg",
      idPlanet: 1
    },
    {
      id: 3,
      name: "Android 17",
      description: "Androide Número 17 (人造人間１７号Kan, Jinzōningen Jū Nana-GōHepJP) conocido como Lapis (ラピスKan, RapisuHepJP)[13] antes de ser secuestrado, es el hermano gemelo de la Androide Número 18, quien al igual que ella antes de ser Androide era un humano normal hasta que fueron secuestrados por el Dr. Gero, y es por eso que lo odian. Tras su cambio de humano a Androide, le insertaron un chip con el objetivo de destruir a Son Goku, quien en su juventud extermino al Ejército del Listón Rojo. Al considerarse defectuoso porque no quería ser 'esclavo de nadie', el Dr. Gero les había colocado a ambos hermanos, un dispositivo de apagado para detenerlos en cualquier momento de desobediencia, pero cuando el científico los despierta, el rencor y rebeldía de 17 eran tal que se encargó de destruir el control que lo volvería a encerrar y acto seguido mató sin piedad a su propio creador. Su situación se le iría en contra, ya que Cell tenía como misión absorber a 17 y 18 para completar su desarrollo y convertirse en Cell Perfecto.",
      avatar: "https://sm.ign.com/t/ign_es/screenshot/default/c66bfac9bc20_emhq.1280.jpg",
      idPlanet: 2
    },    {
      id: 4,
      name: "Trunks",
      description: "Trunks (トランクスKan, TorankusuHepJP), también conocido como Trunk en el doblaje al español de España, es un mestizo entre humano terrícola y Saiyan nacido en la Tierra, e hijo de Bulma y Vegeta, el cual es introducido en el Arco de los Androides y Cell. Más tarde en su vida como joven, se termina convirtiendo en un luchador de artes marciales, el mejor amigo de Son Goten y en el hermano mayor de su hermana Bra.",
      avatar: "https://sm.ign.com/t/ign_es/screenshot/default/c66bfac9bc20_emhq.1280.jpg",
      idPlanet: 2
    },
    {
      id: 5,
      name: "Son Gohan",
      description: "Son Gohan (孫そん悟ご飯はんKan, Son GohanHepJP), Son Gohanda[29] en su tiempo en España, o simplemente Gohan en Hispanoamérica, es uno de los personajes principales de los arcos argumentales de Dragon Ball Z, Dragon Ball Super y Dragon Ball GT. Es un mestizo entre saiyano y humano terrícola. Es el primer hijo de Son Goku y Chi-Chi, hermano mayor de Son Goten, esposo de Videl y padre de Pan.",
      avatar: "https://sm.ign.com/t/ign_es/screenshot/default/c66bfac9bc20_emhq.1280.jpg",
      idPlanet: 2
    }
  ]

  //////////Piezas de lego///////////
  fila:any = 0;
  columna:any = 0;
  colorPieza:any = "white";
  estilo:any = "circulo";

  ////////////////////////////////to do ////////////////////////

  todoList:any = [
    {name: 'Limpiar la casa', isDone: true},
    {name: 'Sacar al gato', isDone: false},
    {name: 'Pintar pieza de Warhammer', isDone: true},
    {name: 'Jugar una partidica', isDone: false},
    ]
    
  item:any = {
    name:"",
    isDone: false
  };
  
  nombreItem:string = "";
 
  
  guardarTarea(){
    //console.log("entrando a tarea")
    //console.log(this.nombreItem)
    if(!this.nombreItem){
      return
    }
    this.todoList.push({'name': this.nombreItem,'isDone':true});
    this.nombreItem="";
  }
  cambiarEstado(){
    console.log("entrando en estado")
    console.log("item name:   " + this.nombreItem)
  }



}


interface Character{
  id:number,
  name:string,
  description:string,
  avatar:string,
  idPlanet:number
}
