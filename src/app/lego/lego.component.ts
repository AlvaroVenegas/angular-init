import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-lego',
  templateUrl: './lego.component.html',
  styleUrls: ['./lego.component.scss']
})
export class LegoComponent implements OnInit {

  @Input() fila: any;
  @Input() columna: any;
  @Input() colorPieza: any;
  @Input() estilo:any;

  constructor() { }

  ngOnInit(): void {
   // this.fila = new Array(Number(this.fila))
    //this.columna = new Array(Number(this.columna))
    //this.colorPieza = String(this.colorPieza)

  }

  ngOnChanges(changes: any) {
    //this.fila = new Array (Number (this.fila))
    //this.columna = new Array(Number (this.columna))
    if (changes.columna) {
      this.columna = new Array(Number(changes.columna.currentValue))
    }
    if (changes.fila) {
      this.fila = new Array(Number(changes.fila.currentValue))
    }
  }
}
