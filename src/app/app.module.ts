import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ButtonComponent } from './button/button.component';
import { HelloWorksComponent } from './hello-works/hello-works.component';
import { InputButtonComponent } from './input-button/input-button.component';
import { CardComponent } from './card/card.component';
import { PersonComponent } from './person/person.component';
import { OverAgeComponent } from './over-age/over-age.component';
import { FormsModule} from '@angular/forms';
import { NgModelComponent } from './ng-model/ng-model.component';
import { NgForComponent } from './ng-for/ng-for.component';
import { CharacterComponent } from './character/character.component';
import { CharacterListComponent } from './character-list/character-list.component';
import { CharacterPruebaComponent } from './character-prueba/character-prueba.component';
import { LegoComponent } from './lego/lego.component';
import { ToDoListComponent } from './to-do-list/to-do-list.component';

@NgModule({
  declarations: [
    AppComponent,
    ButtonComponent,
    HelloWorksComponent,
    InputButtonComponent,
    CardComponent,
    PersonComponent,
    OverAgeComponent,
    NgModelComponent,
    NgForComponent,
    CharacterComponent,
    CharacterListComponent,
    CharacterPruebaComponent,
    LegoComponent,
    ToDoListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
