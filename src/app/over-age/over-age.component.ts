//pasadle un input que sea una edad y si l edad es mayor o igual que 18 muestra el mensaje: 
//Puedes pasar
//Y si la edad es mejor muestra el mensaje:
//No puedes pasar, te faltan un par de petisui


import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-over-age',
  templateUrl: './over-age.component.html',
  styleUrls: ['./over-age.component.scss']
})
export class OverAgeComponent implements OnInit {

  @Input() age:number = 5;

  constructor() { }

  ngOnInit(): void {
  }

}
