import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-to-do-list',
  templateUrl: './to-do-list.component.html',
  styleUrls: ['./to-do-list.component.scss']
})
export class ToDoListComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  @Input() list:TodoItem[] = [];

  @Input() item:TodoItem = {
    name:"",
    isDone: false
  };

}

interface TodoItem{
  name:string;
  isDone:boolean;
}