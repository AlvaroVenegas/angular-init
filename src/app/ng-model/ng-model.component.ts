import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-model',
  templateUrl: './ng-model.component.html',
  styleUrls: ['./ng-model.component.scss']
})
export class NgModelComponent implements OnInit {

  robot:Robot = {
    name : "",
    img : "",
    material : ""
  };

  constructor() { }

  ngOnInit(): void {
  }

}

interface Robot {
    name :string,
    img : string,
    material : string
}